// ohlcChartServices.tsx
import { API_URL,BASE_API_URL, TIME_FRAME } from "../constant/constant";

export const fetchChartData = async (url:string) => {
  try {
    const response = await fetch(url, {
      method: 'GET',
      mode: 'cors',
    });
    const cdata = await response.json();
    const candlestickData = cdata.map((entry: any) => ({
      x: new Date(entry[0]),
      y: [entry[1], entry[3], entry[4], entry[2]], 
    }));
    return candlestickData;
  } catch (error) {
    console.error("Error fetching chart data:", error);
    return [];
  }
};
