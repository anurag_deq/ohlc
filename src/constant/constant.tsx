export const API_URL = "https://api-pub.bitfinex.com/v2/candles/trade:1m:tBTCUSD/hist";
export const BASE_API_URL = "https://api-pub.bitfinex.com/v2/candles/trade:";
export const TIME_FRAME = ["1m", "5m", "15m", "30m", "1h", "3h", "6h", "12h", "1D", "1W", "14D", "1M"]
