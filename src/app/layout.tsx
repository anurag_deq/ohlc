import "./globals.css";
import type { Metadata } from "next";
import Head from "next/head";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "OHLC Chart",
  description: "OHLC",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <Head>
      <link rel="icon" href="/favicon.ico" sizes="any" />
      </Head>
      <body
        style={{
          margin: 0,
          background: "#172D3E",
          color: "#FFF",
          height: "100vh",
        }}
      >
        {children}
      </body>
    </html>
  );
}
