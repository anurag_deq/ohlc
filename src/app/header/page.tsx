"use client";

import React from "react";
import { useRouter } from "next/navigation";
//MUI
import { Box, Button } from "@mui/material";

interface IHeaderDataItemType {
  title: string;
  path: string;
}

function Header() {
  const router = useRouter();
  const HeaderData: IHeaderDataItemType[] = [
    {
      title: "Apex chart",
      path: "/ohlc-chart",
    },
    {
      title: "Trading view Chart",
      path: "/trading-view",
    },
    {
      title: "Order Book",
      path: "/order-book",
    },
  ];

  const handleClick = (path: string) => {
    router.push(path);
  };

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
      }}
    >
      {HeaderData?.map((item: IHeaderDataItemType, index: number) => (
        <Button
          key={index}
          variant="outlined"
          sx={{ m: 1, color: "#FFF", borderColor: "#FFF" }}
          onClick={() => handleClick(item.path)}
        >
          {item.title}
        </Button>
      ))}
    </Box>
  );
}

export default Header;
