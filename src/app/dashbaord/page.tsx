"use client"
import React from 'react'
import { Container } from '@mui/material'
import TradeChart from '../trading-view/page'
import OrderBook from '../order-book/page'
// import OHLCChart from '../ohlc-chart/page'

interface DashBoardProps {

}


const DashBoard: React.FC<DashBoardProps> = () => {
  return (
   <Container>
       <TradeChart noheader/>
       {/* <OHLCChart noheader/> */}
       <OrderBook noheader rootStyle={{marginTop:10}}/>
   </Container>
  )
}

export default DashBoard