"use client";
import React, { Component } from "react";
//MUI
import { Box, IconButton, Typography } from "@mui/material";
//Library
import { AdvancedRealTimeChart } from "react-ts-tradingview-widgets";
//Assets
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import TradeHeader from "./TradeHeader";

interface IOHLCObjType {
  name: string;
}
interface IOHLCType {
  data: IOHLCObjType[];
}
type TradeChartPropType = {
  noheader: boolean;
};

export default class TradeChart extends Component<
  TradeChartPropType,
  IOHLCType
> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: [],
    };
  }
  render() {
    const { noheader } = this.props;
    return (
      <>
      <TradeHeader noheader={noheader}/>
        <Box sx={{ height: "500px", p: 1 }}>
          <AdvancedRealTimeChart
            symbol="BITSTAMP:BTCUSD"
            theme="dark"
            autosize
          />
        </Box>
      </>
    );
  }
}
