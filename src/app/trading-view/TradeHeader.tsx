"use client";
import React from "react";
import { useRouter } from "next/navigation";
//MUI
import {
  Box,
  CircularProgress,
  IconButton,
  Paper,
  Typography,
} from "@mui/material";
//Assets
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import Link from "next/link";


type TradeHeaderProps = {
  noheader?: boolean;
};
const   TradeHeader = ({ noheader }: TradeHeaderProps) => {
  return (
    <Box
    display={noheader ? "none" : "flex"}
    alignItems={"center"}
    justifyContent={"space-between"}
  >
    <Box display="flex" alignItems={"center"}>
      <Link href="/">
        <IconButton title="Back to Home">
          <ArrowBackIcon sx={{ color: "#FFF" }} />
        </IconButton>
      </Link>
      <Typography>Trade Chart</Typography>
    </Box>
  </Box>
  );
};

export default TradeHeader