import Header from "./header/page";
import DashBoard from "./dashbaord/page";
export default function SiteLayout() {
  return (
    <main>
      <Header />
      <DashBoard />
    </main>
  );
}
