"use client";
//MUI
import { Box } from "@mui/material";
//Assets

type BidTableProps = {
  orderBook: { asks: []; bids: [] };
  bidTotalQuantity: number[];
};
const BidTable = ({ orderBook, bidTotalQuantity }: BidTableProps) => {
  return (
    <Box
      sx={{
        position: "relative",
        background: "#172D3E",
        color: "#FFF",
        minHeight: "573px",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "25px",
          transform: "rotateY(180deg)",
          width: "100%",
        }}
      >
        {orderBook.bids.map((bid: any, index: any) => (
          <div
            key={`${bid.amount}-${bid.count}-${bid.price}`}
            style={{
              width: `${
                (bidTotalQuantity[index] /
                  bidTotalQuantity[bidTotalQuantity.length - 1]) *
                100
              }%`,
              height: "22px",
              background: "#73D273",
              opacity: "30%",
            }}
          />
        ))}
      </Box>
      <table
        style={{
          background: "transparent ",
          width: "250px",
          borderRadius: 4,
          padding: 1,
          fontFamily: "monospace",
        }}
      >
        <thead>
          <tr>
            <th>Count</th>
            <th>Amount</th>
            <th>Total</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody style={{ textAlign: "center" }}>
          {orderBook.bids.map((bid: any, index: any) => (
            <tr key={`${bid.amount}-${bid.price}`} style={{ height: "20px" }}>
              <td>{bid.count}</td>
              <td>{Math.abs(bid.amount).toFixed(3)}</td>
              <td>{bidTotalQuantity[index]}</td>
              <td>{bid.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </Box>
  );
};

export default BidTable;
