"use client";
//MUI
import { Box } from "@mui/material";
//Assets

type AskTableProps = {
  orderBook: { asks: []; bids: [] };
  askTotalQuantity: number[];
};
const AskTable = ({ orderBook, askTotalQuantity }: AskTableProps) => {
  return (
    <Box
      sx={{
        position: "relative",
        background: "#172D3E",
        color: "#FFF",
        minHeight: "573px",
      }}
    >
      <Box
        sx={{
          position: "absolute",
          top: "25px",
          // right: "0px",
          width: "100%",
        }}
      >
        {orderBook.asks.map((ask: any, index: any) => (
          <div
            key={`${ask.amount}-${ask.count}-${ask.price}`}
            style={{
              width: `${
                (askTotalQuantity[index] /
                  askTotalQuantity[askTotalQuantity.length - 1]) *
                100
              }%`,
              height: "22px",
              background: "#E45028",
              transform: "rotateY(180deg)",
              opacity: "30%",
            }}
          />
        ))}
      </Box>
      <table
        style={{
          background: "transparent ",
          width: "250px",
          borderRadius: 4,
          padding: 1,
          fontFamily: "monospace",
        }}
      >
        <thead>
          <tr>
            <th>Price</th>
            <th>Total</th>
            <th>Amount</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody
          style={{
            textAlign: "center",
          }}
        >
          {orderBook.asks.map((asks: any, index: any) => (
            <tr key={`${asks.amount}-${asks.price}`} style={{ height: "20px" }}>
              <td>{asks.price}</td>
              <td>{askTotalQuantity[index]}</td>
              <td>{Math.abs(asks.amount).toFixed(3)}</td>
              <td>{asks.count}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </Box>
  );
};

export default AskTable;
