"use client";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
//MUI
import { Box, CircularProgress, Paper } from "@mui/material";
//Assets
//utils
import calculateQuantities from "../../utils/calculateOrderCompondingValue";
//Component
import AskTable from "./AskTable";
import BidTable from "./BidTable";
import OrderHeader from "./orderHeader";

export default function OrderBook(props: any) {
  const { noheader = false, rootStyle = {} } = props;
  const router = useRouter();
  const [orderBook, setOrderBook] = useState<{ bids: any; asks: any }>({
    bids: [],
    asks: [],
  });

  const bidTotalQuantity = calculateQuantities(orderBook?.bids);
  const askTotalQuantity = calculateQuantities(orderBook?.asks);

  useEffect(() => {
    const ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");

    ws.onopen = () => {
      ws.send(
        JSON.stringify({
          event: "subscribe",
          channel: "book",
          symbol: "tBTCUSD",
          prec: "P0",
          freq: "F0",
          len: 25,
        })
      );
    };

    ws.onmessage = (event) => {
      const response = JSON.parse(event.data as string);

      if (Array.isArray(response)) {
        const [_, data] = response;

        const [price, count, amount] = data;
        if (count > 0) {
          setOrderBook((prevOrderBook) => {
            const updatedBids = [...prevOrderBook.bids];
            const updatedAsks = [...prevOrderBook.asks];
            const existingBidIndex = updatedBids.findIndex(
              (bid) => bid.price === price
            );
            const existingAskIndex = updatedAsks.findIndex(
              (ask) => ask.price === price
            );

            if (existingBidIndex !== -1) {
              updatedBids[existingBidIndex] = { price, count, amount };
            } else if (existingAskIndex !== -1) {
              updatedAsks[existingAskIndex] = { price, count, amount };
            } else if (amount > 0) {
              updatedBids.push({ price, count, amount });
            } else {
              updatedAsks.push({ price, count, amount });
            }
            return {
              bids: updatedBids,
              asks: updatedAsks,
            };
          });
        } else {
          setOrderBook((prevOrderBook) => {
            const updatedBids = prevOrderBook.bids.filter(
              (bid: any) => bid.price !== price
            );
            const updatedAsks = prevOrderBook.asks.filter(
              (ask: any) => ask.price !== price
            );

            return {
              bids: updatedBids,
              asks: updatedAsks,
            };
          });
        }
      }
    };

    return () => {
      ws.close();
    };
  }, []);

  const handelBack = () => {
    router.push("./");
  };

  return (
    <Box>
      <OrderHeader handelBack={handelBack} visible={noheader ? "none" : ""} />
      <Box
        p={1}
        
        sx={{
          display:{xs:"block",sm:"flex"}, 
          justifyContent: "center",
          ...rootStyle,
        }}
      >
        {orderBook?.bids?.length === 0 && orderBook?.asks?.length === 0 ? (
          <CircularProgress />
        ) : (
          <>
            <Paper elevation={12}>
              <BidTable
                orderBook={orderBook}
                bidTotalQuantity={bidTotalQuantity}
              />
            </Paper>
            <Paper elevation={12}>
              <AskTable
                orderBook={orderBook}
                askTotalQuantity={askTotalQuantity}
              />
            </Paper>
          </>
        )}
      </Box>
    </Box>
  );
}
