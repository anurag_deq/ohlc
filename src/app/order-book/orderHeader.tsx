"use client";
import React from "react";
import { useRouter } from "next/navigation";
//MUI
import {
  Box,
  CircularProgress,
  IconButton,
  Paper,
  Typography,
} from "@mui/material";
//Assets
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

type OrderHeaderProps = {
  handelBack: () => void;
  visible: string;
};

const   OrderHeader = ({ handelBack, visible }: OrderHeaderProps) => {
  return (
    <Box
      display={visible}
      sx={{
        borderBottom: "1px solid #e7e7e7",
      }}
    >
      <Box display="flex" alignItems={"center"}>
        <IconButton title="Back to Home" onClick={() => handelBack()}>
          <ArrowBackIcon sx={{ color: "#fff" }} />
        </IconButton>
        <Typography fontWeight={800} align="center" flex={1}>
          Order book
        </Typography>
      </Box>
    </Box>
  );
};

export default OrderHeader