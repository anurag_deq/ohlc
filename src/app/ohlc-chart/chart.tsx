import React from 'react';
import dynamic from "next/dynamic";


const DynamicChartComponent = dynamic(() => import('react-apexcharts'), {
  ssr: false, 
});


interface CandlestickChartProps {
  series: { name:string,data: {x:[],y:[]}[] }[];
  options: any;
}

const CandlestickChart: React.FC<CandlestickChartProps> = ({ series, options }) => {
  return (
      <DynamicChartComponent options={options} series={series} type="candlestick" height={350} 
      style={{borderRadius:5,overflow:"hidden",padding:15}}

      />
  );
};

export default CandlestickChart;
