"use client";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import dynamic from "next/dynamic";
const CandelChart = dynamic(() => import("./chart"));

//MUI
import {
  Box,
  Select,
  IconButton,
  Typography,
  MenuItem,
  CircularProgress,
  Link,
  OutlinedInput,
} from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
//utility libraries
import moment from "moment";
//services
import { fetchChartData } from "../../services/ohlcChartServices";
import { API_URL, BASE_API_URL, TIME_FRAME } from "../../constant/constant";






const OHLCChart = (props:any) => {
  const router = useRouter();
  const { noheader = false} = props

  const [selectedTradingPair, setSelectedTradingPair] = useState<string>("tBTCUSD");
  const [selectedTimeFrame, setSelectedTimeFrame] = useState<string>("15m");
  const [loading, setLoading] = useState<boolean>(false);

  const [chartData, setChartData] = useState<{
    series: { name:string,data: {x:[],y:[]}[] }[];
    options: any;
  }>({
    series: [
      {
        name: "candle",
        data: [],
      },
    ],
    options: {
      chart: {
        height: 350,
        type: "candlestick",
        background: "#172d3e",
        foreColor: "#ffffff",
        style: {
          borderRadius: "50px",
        },
      },
      title: {
        text: `${selectedTradingPair.substring(1)} Chart`,
        align: "left",
      },
      tooltip: {
        enabled: true,
        theme: "dark", 
        style: {
          background: "#333", 
          color: "#FFF"
        },
      },
      xaxis: {
        type: "category",
        labels: {
          formatter: function (val: any) {
            return moment(val).format("MMM DD HH:mm");
          },
        },
      },
      yaxis: {
        tooltip: {
          enabled: true,
        },
      },
    },
  });

  const timeFrames = [
    { value: "1m", label: "1m" },
    { value: "5m", label: "5m" },
    { value: "15m", label: "15m" },
    { value: "30m", label: "30m" },
    { value: "1h", label: "1h" },
    { value: "3h", label: "3h" },
    { value: "6h", label: "6h" },
    { value: "12h", label: "12h" },
    { value: "1D", label: "1D" },
    { value: "1W", label: "1W" },
    { value: "14D", label: "14D" },
    { value: "1M", label: "1M" },
  ];

  
  const fetchData = async () => {
    const URL = `${BASE_API_URL}${selectedTimeFrame}:${selectedTradingPair}/hist`;
    setLoading(true);

    const candlestickData = await fetchChartData(URL);
    setChartData({
      ...chartData,
      series: [
        {
          ...chartData.series[0],
          data: candlestickData,
        },
      ],
    });
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, [selectedTradingPair, selectedTimeFrame]);


  return (
    <>
      <Box
        display={noheader?"none":"flex"} 
        alignItems={"center"}
        justifyContent={"space-between"}
      >
        <Box 
        display="flex" 
        alignItems={"center"}>
          <Link href="./">
          <IconButton title="Back to Home">
            <ArrowBackIcon sx={{
            color:"#FFF",borderColor:"#FFF" 
            }}/>
          </IconButton>
          </Link>
          <Typography>Candel Stick Chart</Typography>
        </Box>
        <Box
          display="flex" 
        >
          <Select
            size="small"
            // variant="outlined"
            value={selectedTradingPair}
            onChange={(e) => setSelectedTradingPair(e.target.value)}
            sx={{
              m:2,
              color: "#FFF", 
              border:"1px solid #FFF",
              backgroundColor: "transparent", 
              "& .MuiSelect-icon": {
                color: "#FFF",
              },
              "& .MuiOutlinedInput-root": {
                borderColor: "#FFF", 
              },
              "& .MuiSelect-outlined": {
                "&:before": {
                  borderColor: "#FFF", 
                },
                "&:after": {
                  borderColor: "#FFF",
                },
                "&:hover:not(.Mui-disabled):before": {
                  borderColor: "#FFF",
                },
              },
            }}
          >
            <MenuItem value="tBTCUSD">BTCUSD</MenuItem>
            <MenuItem value="tLTCUSD">LTCUSD</MenuItem>
            <MenuItem value="tETHUSD">ETHUSD</MenuItem>
          </Select>
          <Select
            size="small"
            value={selectedTimeFrame}
            onChange={(e) => setSelectedTimeFrame(e.target.value)}
            sx={{
              m:2,
              border:"1px solid #FFF",
              color: "#FFF", 
              backgroundColor: "transparent", 
              "& .MuiSelect-icon": {
                color: "#FFF",
              },
              "& .MuiOutlinedInput-root": {
                borderColor: "#FFF", 
              },
              "& .MuiSelect-outlined": {
                "&:before": {
                  borderColor: "#FFF", 
                },
                "&:after": {
                  borderColor: "#FFF",
                },
                "&:hover:not(.Mui-disabled):before": {
                  borderColor: "#FFF",
                },
              },
            }}
          >
            {timeFrames.map((timeFrame) => (
              <MenuItem key={timeFrame.value} value={timeFrame.value}>
                {timeFrame.label}
              </MenuItem>
            ))}
          </Select>
        </Box>
      </Box>
      <Box>
        {loading ? (
          <Box display="flex" alignItems={"center"} justifyContent={"center"}>
            <CircularProgress />
          </Box>
        ) : (
          <CandelChart series={chartData.series} options={chartData.options} />
        )}
      </Box>
    </>
  );
};

export default OHLCChart;
