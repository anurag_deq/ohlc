const calculateQuantities = (orders: any) => {
    let quantity = 0;
    const quantities: number[] = [];
  
    for (const order of orders) {
      const { amount } = order;
      quantity += Math.abs(amount);
      quantities.push(Number(quantity.toFixed(3)));
    }
  
    return quantities;
  };
  
  export default calculateQuantities;
  